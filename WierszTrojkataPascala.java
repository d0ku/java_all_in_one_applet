/**
 * WierszTrojkataPascala, oblicza i wyprowadza konkretny wiersz
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.0
 */

class WierszTrojkataPascalaException extends Exception{

	WierszTrojkataPascalaException(String s){
		super(s);
	}
}

public class WierszTrojkataPascala {
	private int n;
	private int[] arr;

	WierszTrojkataPascala(int n)throws WierszTrojkataPascalaException {

		this.n = n;
		if(n<0)
			throw new WierszTrojkataPascalaException("Argument musi byc co najmniej zerem");
		if(n>33)
			throw new WierszTrojkataPascalaException("Zbyt wysoki argument");


		this.arr = new int[n + 1];

		for (int i = 0; i <= n; i++) {
			this.arr[0] = 1;
			this.arr[i] = 1;

			for (int j = i - 1; j > 0; j--) {
				this.arr[j] = this.arr[j] + this.arr[j - 1];
			}
		}
	}

	public String wiersz() {
		String verse = " ";
		char temp = ' ';
		char advanced_temp = ' ';
		int a = 10;

		for (int i = 0; i < this.n + 1; i++) {
		
			verse=verse+this.arr[i]+" ";
		}
		
		return verse;
	}

	public int wspolczynnik(int m) {
		
		return this.arr[m];
	}

}