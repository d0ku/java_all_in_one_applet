/** RSA_main, groups all methods neede to decrypt RSA , first parameter is first argument of private key, second parameter is second parameter of private key,third parameter declares how long is one char, fourth parameter is coded string, it returns decoded string
@author d0ku (Jakub Piątkowski)
@version 1.0
*/
import java.math.*;

class RSAException extends Exception{

	RSAException(String s){
		super(s);
	}
}

public class RSA_main{

	public static String RSA_decrypt(String[] args) throws RSAException{
		int arg_1=-1;
		int arg_2=-1;
		Boolean succes=true;

		if(args.length!=3){
			succes=false;
			throw new RSAException("Zla ilosc argumentow (potrzebne sa 3)");
			
		}

		try{
			arg_1=Integer.parseInt(args[0]);
			arg_2=Integer.parseInt(args[1]);
		}

		catch(NumberFormatException e){
			succes=false;
			throw new RSAException("Klucz publiczny i prywatny musi być liczbą naturalna!!!");
			
		}

		if(arg_1<=1 || arg_2<=1){
			succes=false;
			throw new RSAException("Klucz publiczny i prywatny musi byc wiekszy niz 1...");
			
		}




		if(succes==true){
			RSA_decrypt a= new RSA_decrypt(arg_1,arg_2);
			String[] encrypted= a.RSA_array_conversion(args[2]);
			return a.core(encrypted);
		}
		return "ERROR";

	}
}

//{"03f824fd","033c7a71","050a6706","050a6706","03ffab5e","03f824fd","0189a78d","005bca7d","00734305","04046ca6","017698b6","005bca7d","03f824fd","03d10ac0","003622e4","011c1c7e","030cf03c","011c1c7e","03d10ac0","01b60e5d","03f824fd","00734305","007a18e6","03ffab5e","00734305","0179f797","037906bb","050a6706","007a18e6","015d897d","03f824fd","037906bb","03f824fd","0451f198","059ff1e0","03d10ac0","02e6b154","037906bb","03f824fd","00734305","003622e4","011c1c7e","0414fa45","03f824fd","00a6891a","042edbee"};
//args[0]=2062465;
//args[1]=101080891;
//args[2]=9;  nieaktualne
//args[3]=03f824fd:033c7a71:050a6706:050a6706:03ffab5e:03f824fd:0189a78d:005bca7d:00734305:04046ca6:017698b6:005bca7d:03f824fd:03d10ac0:003622e4:011c1c7e:030cf03c:011c1c7e:03d10ac0:01b60e5d:03f824fd:00734305:007a18e6:03ffab5e:00734305:0179f797:037906bb:050a6706:007a18e6:015d897d:03f824fd:037906bb:03f824fd:0451f198:059ff1e0:03d10ac0:02e6b154:037906bb:03f824fd:00734305:003622e4:011c1c7e:0414fa45:03f824fd:00a6891a:042edbee

class RSA_decrypt{

	private int modulo;
	private int decryptor;

	RSA_decrypt(int a, int b){
		modulo=b;
		decryptor=a;

	}

  public String[] RSA_array_conversion(String a) throws RSAException{
  	int temp =0;

    String [] codes = a.split(":");

    temp = codes[0].length();

    for (int i=0;i<codes.length;i++){ // nie jest to konieczne jeżeli mamy niepełny kod do zdekodowania
    	if(temp!=codes[i].length())
    		throw new RSAException("Zaszyfrowany kod posiada braki");
    }

    return codes;
  }
  
	public int hex2Decimal(String s) {
        String digits = "0123456789ABCDEF";
        s = s.toUpperCase();
        int val = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            int d = digits.indexOf(c);
            val = 16*val + d;
        }
        return val;
    }

    public int Decimal2Ascii(int a){
      BigInteger bi1, bi2, bi3;

      BigInteger exponent = new BigInteger("2");
      exponent=BigInteger.valueOf(this.decryptor);

      bi1 = new BigInteger("7");
      bi1= BigInteger.valueOf(a);
      bi2 = new BigInteger("20");
      bi2=BigInteger.valueOf(this.modulo);

      // perform modPow operation on bi1 using bi2 and exp
      bi3 = bi1.modPow(exponent, bi2);

      a=bi3.intValue();

      // print bi3 value
      return a;
  
    }

   

 	public String core(String[] args) {
		
 		int n;
 		char m;
 		String text=" ";
		for (int i=0;i<args.length;i++){
			n=hex2Decimal(args[i]);
			n=Decimal2Ascii(n);
			m= (char) n;
			text= text +m;

		}
		return text;
	}

}