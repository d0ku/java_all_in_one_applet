 
import java.math.*;

class LowerOrEqualZeroException extends Exception{
	LowerOrEqualZeroException(String s){
		super(s);
	}
}
class Pierwiastek{

	public static double stopien(double a,int b)throws LowerOrEqualZeroException{
		double temp=-1;

		if(a<0 && b%2==0)
			throw new LowerOrEqualZeroException("Argument "+a+" jest mniejszy lub rowny zero");

		if(b==0)
			throw new LowerOrEqualZeroException("Stopien "+b+" nie moze byc rowny zero");

		if(a==0)
			return 0;


		switch(b){
			case 2: 
				temp= Pierwiastek.drugi_stopien(a);
				break;
			case 3:
				temp= Pierwiastek.trzeci_stopien(a);
				break;
			case 4: 
				temp= Pierwiastek.czwarty_stopien(a);
				break;
		}
	
	temp = BigDecimal.valueOf(temp) //defines precision of root
    .setScale(2, RoundingMode.HALF_UP)
    .doubleValue();

    	return temp;
	
	}

	private static double drugi_stopien(double a){
		double b=0;
		double[] tab= {1000000,100000,10000,1000,100,10,1,0.1,0.001,0.0001,0.000001,0.0000001};


		for (int i=0;i<12;i++){
		while(b*b<=a){
			
			b=b+tab[i];
		}
		if(b*b!=a)
			 b=b-tab[i];
		if(b*b==a)
			 break;
	}

	return b;

	}

	private static double trzeci_stopien(double a){
		double b=0;
		double[] tab= {1000000,100000,10000,1000,100,10,1,0.1,0.001,0.0001,0.000001,0.0000001};


		for (int i=0;i<12;i++){
		while(b*b*b<=a){
			
			b=b+tab[i];
		}
		if(b*b*b!=a)
			 b=b-tab[i];
		if(b*b*b==a)
			 break;
	}

	return b;

	}

	private static double czwarty_stopien(double a){
		double b=0;
		double[] tab= {1000000,100000,10000,1000,100,10,1,0.1,0.001,0.0001,0.000001,0.0000001};


		for (int i=0;i<12;i++){
		while(b*b*b*b<=a){
			
			b=b+tab[i];
		}
		if(b*b*b*b!=a)
			 b=b-tab[i];
		if(b*b*b*b==a)
			 break;
	}

	return b;

	}

}